﻿namespace Autocomplete
{
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using System.Threading.Tasks;

    public sealed class LiveSearch
    {
        private static readonly string[] SimpleWords = File.ReadAllLines(@"dict/words.txt");
        private static readonly string[] MovieTitles = File.ReadAllLines(@"dict/movies.txt");
        private static readonly string[] StageNames = File.ReadAllLines(@"dict/stage names.txt");

        private static readonly SimilarLine[] Results = new SimilarLine[3];

        public static string FindBestSimilar(string example)
        {
            var task1 = Task.Factory.StartNew(() => Results[0] = BestSimilarInArray(SimpleWords, example));
            var task2 = Task.Factory.StartNew(() => Results[1] = BestSimilarInArray(MovieTitles, example));
            var task3 = Task.Factory.StartNew(() => Results[2] = BestSimilarInArray(StageNames, example));
            task1.Wait();
            task2.Wait();
            task3.Wait();
            return BestSimilarInArray(Results.Select(x => x.Line).ToArray(), example).Line;
        }

        public void HandleTyping(HintedControl control)
        {
            Task.Factory.StartNew(() => control.Hint = FindBestSimilar(control.LastWord));
        }

        internal static SimilarLine BestSimilarInArray(string[] lines, string example)
        {
            return lines.Aggregate(
                new SimilarLine
                {
                    Line = string.Empty,
                    Sim = 0
                },
                (best, line) =>
                {
                    var current = new SimilarLine
                    {
                        Line = line,
                        Sim = line.Similarity(example)
                    };

                    if ((current.Sim > best.Sim) || (current.Sim == best.Sim && current.Line.Length < best.Line.Length))
                    {
                        return current;
                    }

                    return best;
                });
        }

        internal struct SimilarLine
        {
            internal string Line;
            internal int Sim;
        }
    }
}
