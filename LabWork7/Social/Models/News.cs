﻿
namespace Social.Models
{
    using System.Collections.Generic;

    public struct News
    {
        public int AuthorId;

        public string AuthorName;

        public List<int> Likes;

        public string Text;

        public News(int authorId,string authorName,List<int> likes,string text)
        {
            AuthorId = authorId;
            AuthorName = authorName;
            Likes = likes;
            Text = text;
        }
    }
}
