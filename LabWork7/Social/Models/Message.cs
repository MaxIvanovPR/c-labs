﻿namespace Social.Models
{
    using System;
    using System.Collections.Generic;

    public struct Message
    {
        public int AuthorId;

        public List<int> Likes;

        public int MessageId;

        public DateTime SendDate;

        public string Text;
    }
}
