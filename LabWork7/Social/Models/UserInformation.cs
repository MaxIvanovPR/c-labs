﻿namespace Social.Models
{
    public struct UserInformation
    {
        public string Name;

        public bool Online;

        public int UserId;

        public UserInformation(string name, bool online, int id)
        {
            Name = name;
            Online = online;
            UserId = id;
        }
    }
}
