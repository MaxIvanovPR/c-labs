﻿namespace Social.Models
{
    using System;

    public struct Friend
    {
        public int FromUserId;

        public DateTime SendDate;

        public int Status;

        public int ToUserId;
    }
}
