﻿namespace Social.Models
{
    using System;

    public struct User
    {
        public DateTime DateOfBirth;

        public int Gender;

        public DateTime LastVisit;

        public string Name;

        public bool Online;

        public int UserId;
    }
}
