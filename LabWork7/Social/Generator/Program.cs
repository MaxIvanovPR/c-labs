﻿namespace Generator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;

    using Newtonsoft.Json;

    using Social.Models;

    public class Program
    {
        private const string PathDirectory = @"../../../Data";
        private const string PathUsers = PathDirectory + "/users.json";
        private const string PathFriends = PathDirectory + "/friends.json";
        private const string PathMessages = PathDirectory + "/messages.json";

        private static readonly List<User> Users = new List<User>(); 
        private static readonly List<Message> Messages = new List<Message>();
        private static readonly List<Friend> Friends = new List<Friend>();

        private static readonly List<int> IdList = new List<int>();
        private static readonly List<int> MsgIdList = new List<int>();
        private static readonly Random Rand = new Random();

        private static readonly List<string> MaleNames = File.ReadAllLines(@"../../Data/Male names.txt").ToList();
        private static readonly List<string> FemaleNames = File.ReadAllLines(@"../../Data/Female names.txt").ToList();

        public static void Main()
        {
            for (var i = 0; i < 100; i++)
            {
                Users.Add(CreateUser());
            }

            for (var i = 0; i < 200; i++)
            {
                Messages.Add(CreateMessage());
            }

            for (var i = 0; i < 1000; i++)
            {
                Friends.Add(CreateFriend());
            }

            File.WriteAllText(PathUsers, JsonConvert.SerializeObject(Users));
            File.WriteAllText(PathFriends, JsonConvert.SerializeObject(Friends));
            File.WriteAllText(PathMessages, JsonConvert.SerializeObject(Messages));
        }

        private static User CreateUser()
        {
            var date = new DateTime(Rand.Next(2010, 2015), Rand.Next(1, 12), Rand.Next(1, 28), Rand.Next(0, 24), Rand.Next(0, 60), Rand.Next(0, 60));
            var gender = Rand.Next(0, 2);
            string name;
            if (gender == 0)
            {
                name = MaleNames[Rand.Next(0, MaleNames.Count)];
                MaleNames.Remove(name);
            }
            else
            {
                name = FemaleNames[Rand.Next(0, FemaleNames.Count)];
                FemaleNames.Remove(name);
            }

            var user = new User
                           {
                               DateOfBirth = new DateTime(Rand.Next(1990, 2010), Rand.Next(1, 12), Rand.Next(1, 27)),
                               Gender = gender,
                               LastVisit = date,
                               Name = name.Trim(),
                               Online = Convert.ToBoolean(Rand.Next(0, 2))
                           };
            int id;
            do
            {
                id = Rand.Next(0, 1000000);
            }
            while (IdList.Contains(id));
            user.UserId = id;
            IdList.Add(id);
            return user;
        }

        private static Message CreateMessage()
        {
            var msg = new Message
                          {
                              AuthorId = (Users.Count == 0) ? 0 : Users[Rand.Next(0, Users.Count)].UserId,
                              Likes = new List<int>(),
                              SendDate = new DateTime(
                                   Rand.Next(2010, 2015),
                                   Rand.Next(1, 12),
                                   Rand.Next(1, 28),
                                   Rand.Next(0, 24),
                                   Rand.Next(0, 60),
                                   Rand.Next(0, 60)),
                                   Text = GetRandomString(Rand.Next(10, 100))
                          };
            int id;
            do
            {
                id = Rand.Next(0, 1000000);
            }
            while (MsgIdList.Contains(id));
            msg.MessageId = id;
            MsgIdList.Add(id);

            var limitLikes = Rand.Next(0, 50);
            for (var i = 0; i < limitLikes; i++)
            {
                do
                {
                    id = IdList[Rand.Next(0, IdList.Count)];
                }
                while (msg.Likes.Contains(id));
                msg.Likes.Add(id);
            }

            return msg;
        }

        private static Friend CreateFriend()
        {
            var friend = new Friend
                             {
                                 SendDate =
                                     new DateTime(
                                     Rand.Next(2010, 2015),
                                     Rand.Next(1, 12),
                                     Rand.Next(1, 28),
                                     Rand.Next(0, 24),
                                     Rand.Next(0, 60),
                                     Rand.Next(0, 60)),
                                 Status = Rand.Next(0, 4)
                             };
            int fromId;
            do
            {
                fromId = Rand.Next(0, 1000000);
            }
            while (!IdList.Contains(fromId));
            friend.FromUserId = fromId;

            int toId;
            do
            {
                toId = Rand.Next(0, 1000000);
            }
            while (!IdList.Contains(toId) | toId == friend.FromUserId);
            friend.ToUserId = toId;

            return friend;
        }

        private static string GetRandomString(int size)
        {
            var builder = new StringBuilder();
            for (var i = 0; i < size; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32(Math.Floor((26 * Rand.NextDouble()) + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
