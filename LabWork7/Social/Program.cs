﻿namespace Social
{
    using System;
    using System.Collections.Generic;

    using Social.Models;

    public class Program
    {
        private const string PathDirectory = @"../../Data";
        private const string PathUsers = PathDirectory + "/users.json";
        private const string PathFriends = PathDirectory + "/friends.json";
        private const string PathMessages = PathDirectory + "/messages.json";

        public static void Main(string[] args)
        {
            var users = new List<string> { "Tino" };
            try
            {
                if (users.Count == 0)
                {
                    throw new Exception("User name not specified.");
                }

                var name = users[0];
                if (string.IsNullOrEmpty(name))
                {
                    throw new Exception("Incorrect name");
                }

                var socialDataSource = new SocialDataSource(PathUsers, PathFriends, PathMessages);

                var userContext = socialDataSource.GetUserContext(users[0]);
                var pageNumber = 6;
                var pressedKey = ConsoleKey.Home;

                do
                {
                    if (pressedKey == ConsoleKey.LeftArrow)
                    {
                        if (--pageNumber == 0)
                        {
                            pageNumber = 6;
                        }
                    }
                    else
                    {
                        if (++pageNumber == 7)
                        {
                            pageNumber = 1;
                        }
                    }

                    Write(userContext, pageNumber);
                }
                while ((pressedKey = Console.ReadKey().Key) != ConsoleKey.Enter);

                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error! " + e.Message);
                Console.ReadKey();
            }
        }

        private static void PrintPageHeader(int numberPage)
        {
            switch (numberPage)
            {
                case 1:
                    {
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.Write("User Info ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("| Friends | Friends Online | Subscribers | Friendship offers | News");
                    }

                    break;
                case 2:
                    {
                        Console.Write("User Info |");
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.Write(" Friends ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("| Friends Online | Subscribers | Friendship offers | News");
                    }

                    break;
                case 3:
                    {
                        Console.Write("User Info | Friends |");
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.Write(" Friends Online ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("| Subscribers | Friendship offers | News");
                    }

                    break;
                case 4:
                    {
                        Console.Write("User Info | Friends | Friends Online |");
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.Write(" Subscribers ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("| Friendship offers | News");
                    }

                    break;
                case 5:
                    {
                        Console.Write("User Info | Friends | Friends Online | Subscribers |");
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.Write(" Friendship offers ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("| News");
                    }

                    break;
                case 6:
                    {
                        Console.Write("User Info | Friends | Friends Online | Subscribers | Friendship offers |");
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        Console.WriteLine(" News");
                        Console.BackgroundColor = ConsoleColor.Black;
                    }

                    break;
            }
        }

        private static void Write(UserContext context, int pageNumber)
        {
            Console.Clear();
            Action<UserInformation> printUser =
               information => Console.WriteLine("{0}", information.Name.PadRight(10));
            PrintPageHeader(pageNumber);
            switch (pageNumber)
            {
                case 1:
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("\nInformation about the user:");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine("Name: {0}", context.User.Name);
                        Console.WriteLine("Age: {0}", DateTime.Now.Year - context.User.DateOfBirth.Year);
                    }

                    break;

                case 2:
                    PrintSection("friends", context.Friends, printUser);
                    break;
                case 3:
                    PrintSection("friends online", context.OnlineFriends, printUser);
                    break;
                case 4:
                    PrintSection("subscribers", context.Subscribers, printUser);
                    break;
                case 5:
                    PrintSection("friendship offers", context.FriendshipOffers, printUser);
                    break;
                case 6:
                    PrintSection("news", context.News, news => Console.WriteLine("Author: {0} Count likes: {1}\n Text: {2}\n", news.AuthorName.PadRight(10), news.Likes.Count, news.Text));
                    break;
            }
        }

        private static void PrintSection<T>(string nameSection, List<T> list, Action<T> print)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nList of {0}:           Count: {1}", nameSection, list.Count);
            Console.BackgroundColor = ConsoleColor.Black;
            if (list.Count == 0)
            {
                Console.WriteLine("Empty.");
            }
            else
            {
                for (var i = 0; i < list.Count; i++)
                {
                    Console.Write("{0}) ", (i + 1).ToString("D2"));
                    print(list[i]);
                }
            }
        }
    }
}
