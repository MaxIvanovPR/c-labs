﻿namespace Social
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Newtonsoft.Json;

    using Social.Models;

    public class SocialDataSource
    {
        private List<User> _users;

        private List<Friend> _friends;

        private List<Message> _messages;

        public SocialDataSource(string pathUsers, string pathFriends, string pathMessages)
        {
            GetUsers(pathUsers);
            GetFriends(pathFriends);
            GetMessages(pathMessages);
        }

        public UserContext GetUserContext(string name)
        {
            var userContext = new UserContext();
            try
            {
                userContext.User = _users.Find(x => x.Name == name);
            }
            catch
            {
                throw new Exception("User with this id not found.");
            }

            userContext.Friends = GetUserFriends(userContext.User);
            userContext.FriendshipOffers = GetUserFriendshipOffers(userContext.User);
            userContext.Subscribers = GetUserSubscribers(userContext.User);
            userContext.OnlineFriends = GetUserOnlineFriends(userContext.User);
            userContext.News = GetUserNews(userContext.User);

            return userContext;
        }

        private IEnumerable<UserInformation> GetFriendshipOffersToUser(IEnumerable<Friend> friends)
        {
            return friends.Join(
                _users,
                friend => friend.FromUserId,
                user => user.UserId,
                (friend, user) => new UserInformation(user.Name, user.Online, user.UserId)).Distinct();
        }

        private IEnumerable<UserInformation> GetFriendshipOffersFromUser(IEnumerable<Friend> friends)
        {
            return friends.Join(
                _users,
                friend => friend.ToUserId,
                user => user.UserId,
                (friend, user) => new UserInformation(user.Name, user.Online, user.UserId)).Distinct();
        }

        private List<UserInformation> GetUserFriends(User userContextUser)
        {
            return GetFriendshipOffersFromUser(
                    _friends.Where(friend => friend.FromUserId == userContextUser.UserId && friend.Status == 2))
                .Concat(
                    GetFriendshipOffersToUser(
                        _friends.Where(friend => friend.ToUserId == userContextUser.UserId && friend.Status == 2)))
                .ToList();
        }

        private List<UserInformation> GetUserFriendshipOffers(User userContextUser)
        {
            return GetFriendshipOffersToUser(
                    _friends.Where(
                        friend => friend.ToUserId == userContextUser.UserId
                                  && (friend.Status == 0 || friend.Status == 1)))
                .ToList();
        }

        private List<UserInformation> GetUserSubscribers(User userContextUser)
        {
            return GetFriendshipOffersToUser(
                _friends.Where(friend => friend.ToUserId == userContextUser.UserId && friend.Status != 2)).ToList();
        }

        private List<UserInformation> GetUserOnlineFriends(User userContextUser)
        {
            return GetFriendshipOffersFromUser(
                    _friends.Where(friend => friend.FromUserId == userContextUser.UserId && friend.Status == 2))
                .Concat(
                    GetFriendshipOffersToUser(
                        _friends.Where(friend => friend.ToUserId == userContextUser.UserId && friend.Status == 2)))
                .Where(user => user.Online).ToList();
        }

        private List<News> GetUserNews(User userContextUser)
        {
            var userFriends =
                GetFriendshipOffersToUser(
                        _friends.Where(friend => friend.ToUserId == userContextUser.UserId && friend.Status == 2))
                    .Concat(GetFriendshipOffersFromUser(
                        _friends.Where(friend => friend.FromUserId == userContextUser.UserId && friend.Status == 2)));
            return _messages.Join(
                userFriends,
                message => message.AuthorId,
                friend => friend.UserId,
                (message, user) => new News
                {
                    AuthorId = message.AuthorId,
                    AuthorName = user.Name,
                    Likes = message.Likes,
                    Text = message.Text
                }).ToList();

        }

        private void GetUsers(string path)
        {
            using (var reader = File.OpenText(path))
            {
                _users = JsonConvert.DeserializeObject<List<User>>(reader.ReadToEnd());
            }
        }

        private void GetFriends(string path)
        {
            using (var reader = File.OpenText(path))
            {
                _friends = JsonConvert.DeserializeObject<List<Friend>>(reader.ReadToEnd());
            }
        }

        private void GetMessages(string path)
        {
            using (var reader = File.OpenText(path))
            {
                _messages = JsonConvert.DeserializeObject<List<Message>>(reader.ReadToEnd());
            }
        }
    }
}
