﻿namespace Social
{
    using System.Collections.Generic;

    using Social.Models;

    public class UserContext
    {
        public User User;

        public List<UserInformation> Friends;

        public List<UserInformation> OnlineFriends;

        public List<UserInformation> FriendshipOffers;

        public List<UserInformation> Subscribers; 

        public List<News> News;
    }
}
